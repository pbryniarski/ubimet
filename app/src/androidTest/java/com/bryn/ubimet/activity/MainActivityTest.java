package com.bryn.ubimet.activity;

import android.support.annotation.StringRes;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.annotation.LargeTest;
import android.text.TextUtils;
import android.view.View;

import com.bryn.ubimet.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.core.deps.guava.base.Preconditions.checkArgument;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

import static org.hamcrest.Matchers.allOf;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void testLocationCorrect() {
        onView(allOf(withId(R.id.location_text), withParent(withId(R.id.header))))
                .check(matches(withText("Kraków")))
                .check(matches(isDisplayed()));
    }

    @Test
    public void testTemperatureDisplayed() {
        scrollToItemWithTextLabel(R.string.temperature_label);
        checkLabelMatches(getString(R.string.temperature_label));
        checkValueMatches("6.7");
    }

    @Test
    public void testTemperatureFeelDisplayed() {
        scrollToItemWithTextLabel(R.string.feel_temp_label);
        checkLabelMatches(getString(R.string.feel_temp_label));
        checkValueMatches("5.1");
    }

    @Test
    public void testRainDisplayed() {
        scrollToItemWithTextLabel(R.string.rain_label);
        checkLabelMatches(getString(R.string.rain_label));
        checkValueMatches("0.66");
    }

    @Test
    public void testWindSpeed() {
        scrollToItemWithTextLabel(R.string.wind_speed_label);
        checkLabelMatches(getString(R.string.wind_speed_label));
        checkValueMatches("7.725");
    }

    @Test
    public void testWindGust() {
        scrollToItemWithTextLabel(R.string.wind_gust_speed);
        checkLabelMatches(getString(R.string.wind_gust_speed));
        checkValueMatches("15.03");
    }

    @Test
    public void testWindDirection() {
        scrollToItemWithTextLabel(R.string.wind_direction);
        checkLabelMatches(getString(R.string.wind_direction));
        checkValueMatches("W");
    }

    @Test
    public void testPressure() {
        scrollToItemWithTextLabel(R.string.pressure_label);
        checkLabelMatches(getString(R.string.pressure_label));
        checkValueMatches("101067.0");
        checkUnitMatches(getString(R.string.pressure_unit));
    }

    @Test
    public void testHumidity() {
        scrollToItemWithTextLabel(R.string.humidity_label);
        checkLabelMatches(getString(R.string.humidity_label));
        checkValueMatches("82");
    }

    @Test
    public void testCloudCover() {
        scrollToItemWithTextLabel(R.string.cloud_cover_label);
        checkLabelMatches(getString(R.string.cloud_cover_label));
        checkValueMatches("100");
    }

    private String getString(@StringRes int string){
        return mActivityRule.getActivity().getString(string);
    }

    private static void checkUnitMatches(String expected) {
        onView(allOf(withItemText(expected),withId(R.id.parameter_unit) )).check(matches(isDisplayed()));
    }

    private static void checkValueMatches(String expected) {
        onView(allOf(withItemText(expected),withId(R.id.parameter_value) )).check(matches(isDisplayed()));
    }

    private static void checkLabelMatches(String expected) {
        onView(allOf(withItemText(expected),withId(R.id.parameter_label) )).check(matches(isDisplayed()));
    }

    private static void scrollToItemWithTextLabel(@StringRes int stringRes) {
        onView(withId(R.id.pinpoint_parameters_recycler_view)).perform(
                scrollTo(hasDescendant(withText(stringRes))));
    }

    private static Matcher<View> withItemText(final String itemText) {
        checkArgument(!TextUtils.isEmpty(itemText), "itemText cannot be null or empty");
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View item) {
                return allOf(
                        isDescendantOfA(isAssignableFrom(RecyclerView.class)),
                        withText(itemText)).matches(item);
            }


            @Override
            public void describeTo(Description description) {
                description.appendText("is isDescendantOfA RV with text " + itemText);
            }
        };
    }

}