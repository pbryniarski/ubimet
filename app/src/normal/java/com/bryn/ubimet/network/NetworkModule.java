package com.bryn.ubimet.network;

import android.content.Context;
import android.util.Log;

import com.bryn.ubimet.BuildConfig;
import com.squareup.okhttp.OkHttpClient;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

@Module
public class NetworkModule {

    private final Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }


    @Provides
    @Singleton
    OkHttpClient getHttpClient(SSLSocketFactory sslSocketFactory) {
        OkHttpClient okHttpClient = new OkHttpClient();

        if(BuildConfig.DEBUG) {
            okHttpClient.interceptors().add(new LoggingInterceptor());
        }

        okHttpClient.setSslSocketFactory(sslSocketFactory);
        okHttpClient.setReadTimeout(20000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(20000, TimeUnit.MILLISECONDS);

        okHttpClient.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return "met-api.ubimet.at".equals(arg0);
            }
        });

        return okHttpClient;
    }

    @Provides
    @Singleton
    SSLSocketFactory getSSLContext(Context context) {
        try {
            TrustManager[] trustManagers = new TrustManager[]{
                    new ReloadableX509TrustManager(context)
            };
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManagers, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception ignore) {
            Log.e("MODULE", "ERRO", ignore);
        }
        return null;
    }

    @Provides
    @Singleton
    UbimetApi getApi(OkHttpClient client) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://met-api.ubimet.at:8090/v2")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(UbimetApi.class);
    }
}
