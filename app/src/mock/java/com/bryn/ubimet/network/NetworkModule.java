package com.bryn.ubimet.network;

import android.content.Context;
import android.util.Log;

import com.bryn.ubimet.BuildConfig;
import com.squareup.okhttp.OkHttpClient;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

@Module
public class NetworkModule {

    private final Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }


    @Provides
    @Singleton
    UbimetApi getApi() {
        return new MockUbimetApi();
    }
}
