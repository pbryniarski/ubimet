package com.bryn.ubimet.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SharedPreferencesUtil {

    private final SharedPreferences preferences;

    private static final String LATITUDE = "LATITUDE";
    private static final String LONGITUDE = "LONGITUDE";
    private static final int DEFAULT_INCORRECT_COORDINATE = 1000;

    @Inject
    public SharedPreferencesUtil(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public Location getSavedLocation(){

        float latitude = preferences.getFloat(LATITUDE, DEFAULT_INCORRECT_COORDINATE);
        float longitude =preferences.getFloat(LONGITUDE,DEFAULT_INCORRECT_COORDINATE);

        if(latitude == DEFAULT_INCORRECT_COORDINATE || longitude == DEFAULT_INCORRECT_COORDINATE){
            return null;
        }
        Location lastLocation = new Location("");
        lastLocation.setLatitude(latitude);
        lastLocation.setLongitude(longitude);
        return lastLocation;
    }

    public void saveLocation(Location location) {
        preferences.edit().putFloat(LATITUDE, (float)location.getLatitude()).apply();
        preferences.edit().putFloat(LONGITUDE, (float)location.getLongitude()).apply();
    }
}
