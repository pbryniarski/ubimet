package com.bryn.ubimet.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.util.SparseArray;

import com.bryn.ubimet.R;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ResourceProvider {

    private Context context;

    private SparseArray<Drawable> weatherIcons = new SparseArray<>();

    @Inject
    public ResourceProvider(Context context) {
        this.context = context;
        weatherIcons.append(1, context.getDrawable(R.drawable.ww_1));
        weatherIcons.append(2, context.getDrawable(R.drawable.ww_2));
        weatherIcons.append(3, context.getDrawable(R.drawable.ww_3));
        weatherIcons.append(4, context.getDrawable(R.drawable.ww_4));
        weatherIcons.append(5, context.getDrawable(R.drawable.ww_5));
        weatherIcons.append(6, context.getDrawable(R.drawable.ww_6));
        weatherIcons.append(7, context.getDrawable(R.drawable.ww_7));
        weatherIcons.append(10, context.getDrawable(R.drawable.ww_10));
        weatherIcons.append(11, context.getDrawable(R.drawable.ww_11));
        weatherIcons.append(12, context.getDrawable(R.drawable.ww_12));
        weatherIcons.append(13, context.getDrawable(R.drawable.ww_13));
        weatherIcons.append(14, context.getDrawable(R.drawable.ww_14));
        weatherIcons.append(15, context.getDrawable(R.drawable.ww_15));
        weatherIcons.append(16, context.getDrawable(R.drawable.ww_16));
        weatherIcons.append(17, context.getDrawable(R.drawable.ww_17));
        weatherIcons.append(19, context.getDrawable(R.drawable.ww_19));
        weatherIcons.append(20, context.getDrawable(R.drawable.ww_20));
        weatherIcons.append(21, context.getDrawable(R.drawable.ww_21));
        weatherIcons.append(22, context.getDrawable(R.drawable.ww_22));
        weatherIcons.append(23, context.getDrawable(R.drawable.ww_23));
        weatherIcons.append(24, context.getDrawable(R.drawable.ww_24));
        weatherIcons.append(25, context.getDrawable(R.drawable.ww_25));
        weatherIcons.append(26, context.getDrawable(R.drawable.ww_26));
        weatherIcons.append(27, context.getDrawable(R.drawable.ww_27));
        weatherIcons.append(28, context.getDrawable(R.drawable.ww_28));
        weatherIcons.append(29, context.getDrawable(R.drawable.ww_29));
    }

    public Drawable getWeatherIcon(int iconNumber){
        return weatherIcons.get(iconNumber);
    }

    public String getString(@StringRes int current_location) {
        return context.getString(current_location);
    }

    public String getTemperatureLabel() {
        return getString(R.string.temperature_label);
    }

    public String getTemperatureUnit() {
        return getString(R.string.temperature_unit);
    }

    public String getFeelTemperatureLabel() {
        return getString(R.string.feel_temp_label);
    }

    public String getRainLabel() {
        return getString(R.string.rain_label);
    }

    public String getRainUnit() {
        return getString(R.string.rain_unit);
    }

    public String getWindSpeedLabel() {
        return getString(R.string.wind_speed_label);
    }

    public String getSpeedUnit() {
        return getString(R.string.speed_label);
    }

    public String getWindGustLabel() {
        return getString(R.string.wind_gust_speed);
    }

    public String getWindDirectionLabel() {
        return getString(R.string.wind_direction);
    }

    public String getPressureLabel() {
        return getString(R.string.pressure_label);
    }

    public String getPressureUnit(){
        return getString(R.string.pressure_unit);
    }

    public String getHumidityLabel() {
        return getString(R.string.humidity_label);
    }

    public String getHumidityUnit() {
        return getString(R.string.percent);
    }

    public String getCloudCoverLabel() {
        return getString(R.string.cloud_cover_label);
    }

    public String getCloudCOverUnit() {
        return getHumidityUnit();
    }

    public String getPinpointTitle() {
        return getString(R.string.current_location_weather_fragment_title);
    }

    public String getPoiTitle() {
        return getString(R.string.poi_weather_fragment_title);
    }

    public String getUnableToLoadCurrentLocationWeatherInfo() {
        return context.getString(R.string.error_no_current_location_weather);
    }

    public String getUnableToLoadPoiForecast() {
        return context.getString(R.string.error_no_poi_forecast);
    }

    public int getProgressBarColor() {
        return context.getResources().getColor(R.color.GreenYellow);
    }

    public String getUnableToGetCurrentLocationInfo() {
        return getString(R.string.unable_to_get_location);
    }

    public String getMainActivityTitle() {
        return getString(R.string.ubimet_weather);
    }

    public String getFullForecastTitle() {
        return getString(R.string.full_forecast_title);
    }


    public String getNoLocationPersmissionString() {
        return getString(R.string.no_location_persmission);
    }
}
