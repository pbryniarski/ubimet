package com.bryn.ubimet.util;

import android.content.res.Resources;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;

import com.bryn.ubimet.R;

import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelbryniarski on 22.11.15.
 */
@Singleton
public class Utilities {

    @Inject
    public Utilities() {
    }

    public String getUserLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public void showSnackBar(View view, String text) {

        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = view.getContext().getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        int color = typedValue.data;
        snackbar.getView().setBackgroundColor(color);

        snackbar.show();
    }
}
