package com.bryn.ubimet.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bryn.ubimet.R;
import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.util.ResourceProvider;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.subjects.PublishSubject;

/**
 * Created by pawelbryniarski on 05.09.15.
 */
public class ForecastAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final LayoutInflater inflater;

    private final View.OnClickListener listener;

    private ArrayList<ForecastNow> forecasts = null;
    private final ResourceProvider resourceProvider;
    private PublishSubject<ForecastNow> clickedForecast = PublishSubject.create();

    public ForecastAdapter(LayoutInflater inflater, ResourceProvider resourceProvider) {
        this.inflater = inflater;
        this.resourceProvider = resourceProvider;
        this.listener = new OnSimpleForecastClickListener();
    }

    public void setForecasts(ArrayList<ForecastNow> forecasts) {
        this.forecasts = forecasts;
        notifyDataSetChanged();
    }

    public PublishSubject<ForecastNow> getClickedForecast() {
        return clickedForecast;
    }

    public ArrayList<ForecastNow> getForecasts() {
        return forecasts;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.location_text)
        TextView locationTextView;

        @Bind(R.id.temperature_text)
        TextView temperatureTextView;

        @Bind(R.id.rain_text)
        TextView rainTextView;

        @Bind(R.id.feel_temperature_text)
        TextView feelTemp;

        @Bind(R.id.wind_speed_text)
        TextView windSpeedTextView;

        @Bind(R.id.weather_icon)
        ImageView weatherIcon;

        View wholeView;

        public ViewHolder(View v) {
            super(v);
            wholeView = v;
            ButterKnife.bind(this, v);
            wholeView.setTag(this);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.simple_forecast, parent, false);
        v.setOnClickListener(listener);
        return new ViewHolder(v);
    }

    public class OnSimpleForecastClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            if (viewHolder != null) {
                clickedForecast.onNext(forecasts.get(viewHolder.getAdapterPosition()));
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;

        ForecastNow forecastNow = forecasts.get(position);
        vh.locationTextView.setText(forecastNow.locationName);
        vh.temperatureTextView.setText(forecastNow.temperature);
        vh.windSpeedTextView.setText(forecastNow.windSpeed);
        vh.rainTextView.setText(forecastNow.precipitation);
        vh.feelTemp.setText(forecastNow.feelTemperature);
        vh.weatherIcon.setImageDrawable(resourceProvider.getWeatherIcon(forecastNow.weatherSymbol));
    }

    @Override
    public int getItemCount() {
        return forecasts == null ? 0 : forecasts.size();
    }

}
