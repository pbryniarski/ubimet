package com.bryn.ubimet.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bryn.ubimet.R;
import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.util.ResourceProvider;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PinpointParamAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final LayoutInflater inflater;
    private ForecastNow forecast;
    private final ResourceProvider resourceProvider;

    public PinpointParamAdapter(LayoutInflater inflater, ResourceProvider resourceProvider) {
        this.inflater = inflater;
        this.resourceProvider = resourceProvider;
    }

    public void setForecast(ForecastNow forecast) {
        this.forecast = forecast;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.parameter_label)
        TextView parameterLabel;

        @Bind(R.id.parameter_value)
        TextView parameterValue;

        @Bind(R.id.parameter_unit)
        TextView parameterUnit;

        View wholeView;

        public ViewHolder(View v) {
            super(v);
            wholeView = v;
            ButterKnife.bind(this, v);
        }

        public void bindTo(String label, String value, String unit){
            parameterLabel.setText(label);
            parameterValue.setText(value);
            parameterUnit.setText(unit);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.pinpoint_forecast_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;

        switch (position){


            case 0:
                vh.bindTo(resourceProvider.getTemperatureLabel(),
                          forecast.temperature,
                        resourceProvider.getTemperatureUnit()
                );
                break;
            case 1:
                vh.bindTo(resourceProvider.getFeelTemperatureLabel(),
                        forecast.feelTemperature,
                        resourceProvider.getTemperatureUnit()
                );
                break;
            case 2:
                vh.bindTo(resourceProvider.getRainLabel(),
                        forecast.precipitation,
                        resourceProvider.getRainUnit()
                );
                break;

            case 3:
                vh.bindTo(resourceProvider.getWindSpeedLabel(),
                        forecast.windSpeed,
                        resourceProvider.getSpeedUnit()
                );
                break;

            case 4:
                vh.bindTo(resourceProvider.getWindGustLabel(),
                        forecast.windGustSpeed,
                        resourceProvider.getSpeedUnit()
                );
                break;

            case 5:
                vh.bindTo(resourceProvider.getWindDirectionLabel(),
                        String.valueOf(forecast.windDirection),
                        ""
                );

                break;

            case 6:
                vh.bindTo(resourceProvider.getPressureLabel(),
                        forecast.pressure,
                        resourceProvider.getPressureUnit()
                );

                break;

            case 7:
                vh.bindTo(resourceProvider.getHumidityLabel(),
                        forecast.humidity,
                        resourceProvider.getHumidityUnit()
                );
                break;

            case 8:
                vh.bindTo(resourceProvider.getCloudCoverLabel(),
                        forecast.cloudCover,
                        resourceProvider.getCloudCOverUnit()
                );
                break;

            default:
                throw new IllegalStateException("Adapter size does not match parameter count");
        }
    }

    @Override
    public int getItemCount() {
        if(forecast == null){
            return 0;
        }

        // we already displayed location name and weather icon
        return forecast.getNumberOfParameters() - 2;
    }
}
