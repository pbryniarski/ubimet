package com.bryn.ubimet.network;

import android.location.Location;
import android.util.Log;

import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.modelconverter.PinpointForecastConverter;
import com.bryn.ubimet.modelconverter.PoiForecastConverter;
import com.bryn.ubimet.modelconverter.PoiInfoConverter;
import com.bryn.ubimet.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by pawelbryniarski on 15.11.15.
 */
@Singleton
public class NetworkForecastProvider {

    private static final String TAG = "NetworkForecastProvider";
    private final Utilities utilities;
    private final UbimetApi api;
    private final PoiInfoConverter poiInfoConverter;
    private final PoiForecastConverter poiForecastConverter;
    private final PinpointForecastConverter pinpointForecastConverter;

    @Inject
    public NetworkForecastProvider(Utilities utilities,
                                   UbimetApi api,
                                   PoiInfoConverter poiInfoConverter,
                                   PoiForecastConverter poiForecastConverter,
                                   PinpointForecastConverter pinpointForecastConverter) {
        this.utilities = utilities;
        this.api = api;
        this.poiInfoConverter = poiInfoConverter;
        this.poiForecastConverter = poiForecastConverter;
        this.pinpointForecastConverter = pinpointForecastConverter;
    }

    public Observable<ArrayList<ForecastNow>> getPoiForecast() {
        String userLanguage = utilities.getUserLanguage();
        return Observable.zip(
                api.getPoiWeather(userLanguage, "basic_now")
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.computation()),

                api.getPoiInfo(userLanguage)
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.computation())
                        .map(poiInfoConverter),
                poiForecastConverter
        )
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Func1<Location, Observable<ForecastNow>> getPinpointForecast() {
        return locationToForecast;
    }

    private final Func1<Location, Observable<ForecastNow>> locationToForecast =
            new Func1<Location, Observable<ForecastNow>>() {
                @Override
                public Observable<ForecastNow> call(Location location) {
                    Log.d(TAG," Location to forecast request");
                    return api.getPinpointWeather(
                            utilities.getUserLanguage(),
                            "basic_now",
                            location.getLongitude() + " " + location.getLatitude())
                            .observeOn(Schedulers.computation())
                            .map(pinpointForecastConverter);
                }
            };
}
