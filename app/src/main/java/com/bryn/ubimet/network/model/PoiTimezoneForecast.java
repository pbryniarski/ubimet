package com.bryn.ubimet.network.model;

/**
 * Created by pawelbryniarski on 12.11.15.
 */
public class PoiTimezoneForecast extends BaseTimezoneForecast {
    public PoiIds spatial_features;
    public class PoiIds {
        public String poi_refs[];
    }
}
