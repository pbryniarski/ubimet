package com.bryn.ubimet.network.model;

/**
 * Created by pawelbryniarski on 10.11.15.
 */
public class BaseTimezoneForecast {

    public String timezone;
    public ForecastSet[] met_sets;

    public class ForecastSet {
        public String name;
        public String begin;
        public Forecast[] parameter_timesets;
    }

    public class Forecast {
        public String [] params;
        public String timebase;
        public double [][][] data;
    }
}
