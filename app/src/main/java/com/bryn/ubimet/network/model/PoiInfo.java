package com.bryn.ubimet.network.model;

/**
 * Created by pawelbryniarski on 12.11.15.
 */
public class PoiInfo {
    public Info spatial_features[];
    public String timezone;

    public static class Info {
        public String id;
        public String name;
    }
}
