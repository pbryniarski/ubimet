package com.bryn.ubimet.network;

import com.bryn.ubimet.network.model.PinpointTimezoneForecast;
import com.bryn.ubimet.network.model.PoiInfo;
import com.bryn.ubimet.network.model.PoiTimezoneForecast;

import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Query;
import rx.Observable;

public interface UbimetApi {

    @Headers({
            "Accept: application/json",
            "Authorization: Token a577d923c0c695d86b2e14c6b6c0cc5be92237e0"
    })
    @GET("/pinpoint-data")
    Observable<PinpointTimezoneForecast[]> getPinpointWeather(
            @Header("Accept-Language") String userLanguage,
            @Query("sets") String sets,
            @Query("coordinates") String coordinate
    );

    @Headers({
            "Accept: application/json",
            "Authorization: Token 60300b89a1ee8dabd0caccd3c8a6f720f4edc178"
    })
    @GET("/poi-data")
    Observable<PoiTimezoneForecast[]> getPoiWeather(
            @Header("Accept-Language") String userLanguage,
            @Query("sets") String sets
    );

    @Headers({
            "Accept: application/json",
            "Authorization: Token 60300b89a1ee8dabd0caccd3c8a6f720f4edc178"
    })
    @GET("/poi-info")
    Observable<PoiInfo[]> getPoiInfo(
            @Header("Accept-Language") String userLanguage
    );
}
