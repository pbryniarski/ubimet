package com.bryn.ubimet.network;

import android.content.Context;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class ReloadableX509TrustManager
        implements X509TrustManager {
    private X509TrustManager trustManager;

    Context context;

    public ReloadableX509TrustManager(Context context)
            throws Exception {
        this.context = context;
        reloadTrustManager();
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain,
                                   String authType) throws CertificateException {
        trustManager.checkClientTrusted(chain, authType);
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain,
                                   String authType) throws CertificateException {
        try {
            trustManager.checkServerTrusted(chain, authType);
        } catch (CertificateException cx) {
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return trustManager.getAcceptedIssuers();
    }

    private void reloadTrustManager() throws Exception {

        // load keystore from specified cert store (or default)
        KeyStore ts = KeyStore.getInstance(
                KeyStore.getDefaultType());
        InputStream in = context.getAssets().open("jssecacerts");
        try {
            ts.load(in, "changeit".toCharArray());
        } finally {
            in.close();
        }

        // initialize a new TMF with the ts we just loaded
        TrustManagerFactory tmf
                = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ts);

        // acquire X509 trust manager from factory
        TrustManager tms[] = tmf.getTrustManagers();
        for (TrustManager tm : tms) {
            if (tm instanceof X509TrustManager) {
                trustManager = (X509TrustManager) tm;
                return;
            }
        }

        throw new NoSuchAlgorithmException(
                "No X509TrustManager in TrustManagerFactory");
    }
}
