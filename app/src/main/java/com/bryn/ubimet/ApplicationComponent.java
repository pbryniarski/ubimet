package com.bryn.ubimet;

import com.bryn.ubimet.activity.FullForecastActivity;
import com.bryn.ubimet.activity.MainActivity;
import com.bryn.ubimet.fragment.PinpointForecastFragment;
import com.bryn.ubimet.fragment.PoiFragment;
import com.bryn.ubimet.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, ApplicationModule.class} )
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);
    void inject(PinpointForecastFragment fragment);
    void inject(PoiFragment fragment);
    void inject(FullForecastActivity fullForecastActivity);
}
