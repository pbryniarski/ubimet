package com.bryn.ubimet;

import android.content.Context;
import android.location.LocationManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Context getAppContext(){
        return context;
    }

    @Provides
    @Singleton
    public LocationManager getLocationManager(Context context){
        return (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
    }
}
