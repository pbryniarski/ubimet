package com.bryn.ubimet;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;

import com.bryn.ubimet.network.NetworkModule;

public class App  extends Application{

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .networkModule(new NetworkModule(getApplicationContext())).build();

        if(BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
