package com.bryn.ubimet.fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bryn.ubimet.App;
import com.bryn.ubimet.R;
import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.presenter.ForecastPresenter;
import com.bryn.ubimet.util.ResourceProvider;
import com.bryn.ubimet.util.Utilities;
import com.bryn.ubimet.view.FullForecastView;

import java.util.concurrent.TimeoutException;

import javax.inject.Inject;

import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class PinpointForecastFragment extends BaseFragment {

    private static final String TAG = "PinpointFragment";
    private static final String FORECAST = "FORECAST";


    @Inject
    ForecastPresenter presenter;

    @Inject
    ResourceProvider resourceProvider;

    @Inject
    Utilities utilities;


    private FullForecastView forecastView;
    private View view;

    public static PinpointForecastFragment newInstance() {
        return new PinpointForecastFragment();
    }

    public PinpointForecastFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((App) getActivity().getApplication()).getApplicationComponent().inject(this);
        view = inflater.inflate(R.layout.fragment_pinpoint, container, false);
        forecastView = new FullForecastView(view, resourceProvider, inflater);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ForecastNow forecastNow = null;
        if (savedInstanceState != null) {
            forecastNow = savedInstanceState.getParcelable(FORECAST);
        }
        if (forecastNow != null) {
            forecastView.bindViews(forecastNow);
        } else {
            refresh();
        }
    }

    @Override
    public void refresh() {
        forecastView.showProgressBar();
        subscription.add(
                presenter.getCurrentLocationForecast().unsubscribeOn(Schedulers.io())
                        .subscribe(new Action1<ForecastNow>() {
                            @Override
                            public void call(ForecastNow forecastNow) {
                                forecastView.bindViews(forecastNow);
                                forecastView.hideProgressBar();
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                forecastView.hideProgressBar();
                                utilities.showSnackBar(view, resourceProvider.getUnableToLoadCurrentLocationWeatherInfo());
                                Log.e(TAG, "Error", throwable);
                                if (throwable instanceof TimeoutException) {
                                    utilities.showSnackBar(view, resourceProvider.getUnableToGetCurrentLocationInfo());
                                } else {
                                    utilities.showSnackBar(view, resourceProvider.getUnableToLoadCurrentLocationWeatherInfo());
                                }
                            }
                        }));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (forecastView.getForecast() != null) {
            outState.putParcelable(FORECAST, forecastView.getForecast());
        }
        super.onSaveInstanceState(outState);
    }
}
