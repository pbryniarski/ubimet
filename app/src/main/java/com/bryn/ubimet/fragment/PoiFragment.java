package com.bryn.ubimet.fragment;


import android.app.Fragment;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bryn.ubimet.App;
import com.bryn.ubimet.R;
import com.bryn.ubimet.activity.FullForecastActivity;
import com.bryn.ubimet.adapter.ForecastAdapter;
import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.network.NetworkForecastProvider;
import com.bryn.ubimet.presenter.ForecastPresenter;
import com.bryn.ubimet.util.ResourceProvider;
import com.bryn.ubimet.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PoiFragment extends BaseFragment {

    private static final String TAG = "PoiFragment";
    private static final String FORECASTS = "FORECASTS";

    @Bind(R.id.forecasts_recycler)
    RecyclerView poiRecyclerView;

    @Bind(R.id.progress_Bar)
    ProgressBar progressBar;

    private View view;

    @Inject
    ResourceProvider resourceProvider;

    @Inject
    NetworkForecastProvider networkForecastProvider;

    @Inject
    ForecastPresenter presenter;

    @Inject
    Utilities utilities;
    private ForecastAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_poi, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((App) getActivity().getApplication()).getApplicationComponent().inject(this);

        poiRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ForecastAdapter(getActivity().getLayoutInflater(), resourceProvider);
        poiRecyclerView.setAdapter(adapter);

        progressBar.getIndeterminateDrawable().setColorFilter(resourceProvider.getProgressBarColor(), PorterDuff.Mode.SRC_IN);
        progressBar.bringToFront();

        subscription.add(adapter.getClickedForecast().subscribe(new Action1<ForecastNow>() {
                    @Override
                    public void call(ForecastNow forecastNow) {
                        startFullForecastActivity(forecastNow);
                    }
                })
        );

        getForecasts(savedInstanceState);
    }

    private void getForecasts(Bundle savedInstanceState) {
        ArrayList<ForecastNow> forecastNows = null;
        if (savedInstanceState != null) {
            forecastNows = savedInstanceState.getParcelableArrayList(FORECASTS);
        }

        if (forecastNows != null) {
            adapter.setForecasts(forecastNows);
        } else {
            refresh();
        }
    }

    private void startFullForecastActivity(ForecastNow forecastNow) {
        getActivity().startActivity(FullForecastActivity.getIntent(getActivity(), forecastNow));
    }

    @Override
    public void refresh() {
        progressBar.setVisibility(View.VISIBLE);

        subscription.add(
                networkForecastProvider.getPoiForecast()
                        .unsubscribeOn(Schedulers.io())
                        .subscribe(new Action1<ArrayList<ForecastNow>>() {
                            @Override
                            public void call(ArrayList<ForecastNow> forecastNow) {
                                adapter.setForecasts(forecastNow);
                                progressBar.setVisibility(View.GONE);
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                utilities.showSnackBar(view, resourceProvider.getUnableToLoadPoiForecast());
                                Log.e(TAG, "Error", throwable);
                            }
                        }));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (adapter.getForecasts() != null) {
            outState.putParcelableArrayList(FORECASTS, adapter.getForecasts());
        }
        super.onSaveInstanceState(outState);

    }

    public static Fragment newInstance() {
        return new PoiFragment();
    }
}
