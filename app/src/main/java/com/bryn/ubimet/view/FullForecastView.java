package com.bryn.ubimet.view;

import android.graphics.PorterDuff;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bryn.ubimet.R;
import com.bryn.ubimet.adapter.PinpointParamAdapter;
import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.util.ResourceProvider;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FullForecastView {

    @Bind(R.id.pinpoint_parameters_recycler_view)
    RecyclerView parametersRecyclerView;

    @Bind(R.id.progress_Bar)
    ProgressBar progressBar;

    @Bind(R.id.location_text)
    TextView locationTextView;

    @Bind(R.id.weather_icon)
    ImageView weatherIcon;

    private final ResourceProvider resourceProvider;
    private final PinpointParamAdapter adapter;

    private ForecastNow forecastNow;

    public FullForecastView(View view, ResourceProvider resourceProvider, LayoutInflater inflater) {
        this.resourceProvider = resourceProvider;
        ButterKnife.bind(this,view);
        parametersRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter = new PinpointParamAdapter(inflater, resourceProvider);
        parametersRecyclerView.setAdapter(adapter);
        progressBar.getIndeterminateDrawable().setColorFilter(resourceProvider.getProgressBarColor(), PorterDuff.Mode.SRC_IN);
    }

    public void bindViews(ForecastNow forecastNow) {
        this.forecastNow = forecastNow;
        locationTextView.setText(forecastNow.locationName);
        weatherIcon.setImageDrawable(resourceProvider.getWeatherIcon(forecastNow.weatherSymbol));
        adapter.setForecast(forecastNow);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public ForecastNow getForecast() {
        return forecastNow;
    }
}
