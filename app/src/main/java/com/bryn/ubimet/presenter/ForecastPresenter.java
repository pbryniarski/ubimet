package com.bryn.ubimet.presenter;

import android.location.Location;
import android.util.Log;

import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.location.LocationProviderChooser;
import com.bryn.ubimet.location.LocationReader;
import com.bryn.ubimet.location.RxGeoCoder;
import com.bryn.ubimet.network.NetworkForecastProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by pawelbryniarski on 22.11.15.
 */
@Singleton
public class ForecastPresenter {

    private static final String TAG = "ForecastPresenter";
    private final LocationReader locationReader;
    private final LocationProviderChooser locationProviderChooser;
    private final NetworkForecastProvider networkForecastProvider;
    private final RxGeoCoder rxGeoCoder;

    @Inject
    public ForecastPresenter(LocationReader locationReader, LocationProviderChooser locationProviderChooser, NetworkForecastProvider networkForecastProvider, RxGeoCoder rxGeoCoder) {
        this.locationReader = locationReader;
        this.locationProviderChooser = locationProviderChooser;
        this.networkForecastProvider = networkForecastProvider;
        this.rxGeoCoder = rxGeoCoder;
    }

    public Observable<ForecastNow> getCurrentLocationForecast(){

        String locationProviderName = locationProviderChooser.getLocationProvider();

        if(locationProviderName == null){
            Log.e(TAG, "No providers available");
        }

        Observable<Location> locationObservable = locationReader.getLocation(locationProviderName).cache();
        return Observable.zip(
                locationObservable.observeOn(Schedulers.io()).map(rxGeoCoder),
                locationObservable.observeOn(Schedulers.io()).switchMap(networkForecastProvider.getPinpointForecast()), new Func2<String, ForecastNow, ForecastNow>() {
                    @Override
                    public ForecastNow call(String s, ForecastNow forecastNow) {
                        forecastNow.locationName = s;
                        return forecastNow;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
    }
}
