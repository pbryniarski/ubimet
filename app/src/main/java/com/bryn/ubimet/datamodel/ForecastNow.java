package com.bryn.ubimet.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pawelbryniarski on 15.11.15.
 */
public class ForecastNow implements Parcelable {

    private static final String TAG = "ForecastNow";

    protected ForecastNow(Parcel in) {
        locationName = in.readString();
        feelTemperature = in.readString();
        windGustSpeed = in.readString();
        temperature = in.readString();
        cloudCover = in.readString();
        humidity = in.readString();
        precipitation = in.readString();
        windDirection = in.readString();
        weatherSymbol = in.readInt();
        windSpeed = in.readString();
        pressure = in.readString();
    }

    protected static String radiansToWindDirectionString(double radians){
        if(radians < 0 || radians > 2 * Math.PI){
            throw new IllegalArgumentException("Wind direction incorrect");
        }

        if(radians <= ((1f/8) *2* Math.PI)  || radians >= ((7f/8) *2*Math.PI) ){
            return "N";
        }

        if(radians <=  ((3f/8) *2* Math.PI) ){
            return "E";
        }

        if(radians <=  ((5f/8) *2* Math.PI) ){
            return "S";
        }

        if(radians <=  ((7f/8) *2* Math.PI) ){
            return "W";
        }

        return "n/a";
    }

    public String locationName;
    public String feelTemperature;
    public String windGustSpeed;
    public String temperature;
    public String cloudCover;
    public String humidity;
    public String precipitation;
    public String windDirection;
    public int weatherSymbol;
    public String windSpeed;
    public String pressure;


    public ForecastNow(String locationName, double feelTemperature, double windGustSpeed,
                       double temperature, double cloudCover, double humidity,
                       double precipitation, double windDirection, int weatherSymbol,
                       double windSpeed, double pressure) {
        this.locationName = locationName;
        this.feelTemperature = String.valueOf(  ((double)((int)(feelTemperature*10) - 2731))/10 );
        this.windGustSpeed = String.valueOf(windGustSpeed);
        this.temperature = String.valueOf(  ((double)((int)(temperature*10) - 2731))/10 );
        this.cloudCover = String.valueOf((int)(cloudCover*100));
        this.humidity = String.valueOf((int)(humidity*100));
        this.precipitation = String.valueOf(((double)((int)(precipitation*1000)))/1000 );
        this.windDirection = radiansToWindDirectionString(windDirection);
        this.weatherSymbol = weatherSymbol;
        this.windSpeed = String.valueOf(windSpeed);
        this.pressure = String.valueOf(pressure);
    }

    @Override
    public String toString() {
        return "ForecastNow{" +
                "locationName='" + locationName + '\'' +
                ", feelTemperature=" + feelTemperature +
                ", windGustSpeed=" + windGustSpeed +
                ", temperature=" + temperature +
                ", cloudCover=" + cloudCover +
                ", humidity=" + humidity +
                ", precipitation=" + precipitation +
                ", windDirection=" + windDirection +
                ", weatherSymbol=" + weatherSymbol +
                ", windSpeed=" + windSpeed +
                ", pressure=" + pressure +
                '}';
    }

    public int getNumberOfParameters(){
        return 11;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(locationName);
        dest.writeString(feelTemperature);
        dest.writeString(windGustSpeed);
        dest.writeString(temperature);
        dest.writeString(cloudCover);
        dest.writeString(humidity);
        dest.writeString(precipitation);
        dest.writeString(windDirection);
        dest.writeInt(weatherSymbol);
        dest.writeString(windSpeed);
        dest.writeString(pressure);
    }

    public static final Creator<ForecastNow> CREATOR = new Creator<ForecastNow>() {
        @Override
        public ForecastNow createFromParcel(Parcel in) {
            return new ForecastNow(in);
        }

        @Override
        public ForecastNow[] newArray(int size) {
            return new ForecastNow[size];
        }
    };


}
