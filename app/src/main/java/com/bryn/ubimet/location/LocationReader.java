package com.bryn.ubimet.location;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.bryn.ubimet.util.SharedPreferencesUtil;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

@Singleton
public class LocationReader {

    private static final String TAG = "NetworkLocationProvider";
    private final LocationManager locationManager;
    private final SharedPreferencesUtil preferences;

    @Inject
    public LocationReader(LocationManager locationManager , SharedPreferencesUtil preferences) {
        this.locationManager = locationManager;
        this.preferences = preferences;
    }

    public Observable<Location> getLocation(final String locationProviderName) {
        return Observable.create(new Observable.OnSubscribe<Location>() {
            @Override
            public void call(final Subscriber<? super Location> subscriber) {
                if(locationProviderName == null){
                   return;
                }

                final LocationListener locationListener = new LocationListener() {
                    public void onLocationChanged(Location location) {

                        Log.d(TAG, "Location altitude : " + location.getAltitude() + " longtitude: " + location.getLongitude() + " latitude: " + location.getLatitude());
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(location);
                            preferences.saveLocation(location);
                            subscriber.onCompleted();
                            locationManager.removeUpdates(this);
                        }
                    }

                    public void onStatusChanged(String provider, int status, Bundle extras) {
                        Log.d(TAG, "Status changed");
                    }

                    public void onProviderEnabled(String provider) {
                        Log.d(TAG, "Provider enabled");
                    }

                    public void onProviderDisabled(String provider) {
                        Log.d(TAG, "Provider disabled");
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(null);
                            subscriber.onCompleted();
                            locationManager.removeUpdates(this);
                        }
                    }
                };
                locationManager.requestLocationUpdates(locationProviderName, 0, 0, locationListener);
            }
        })
                .startWith(preferences.getSavedLocation())
                .filter(locationNotNull)
                .timeout(30, TimeUnit.SECONDS);
    }

    private final Func1<Location, Boolean> locationNotNull =
            new Func1<Location, Boolean>() {
                @Override
                public Boolean call(Location location) {
                    return location != null;
                }
            };
}
