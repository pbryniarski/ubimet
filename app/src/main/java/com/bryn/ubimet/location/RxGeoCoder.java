package com.bryn.ubimet.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.bryn.ubimet.R;
import com.bryn.ubimet.util.ResourceProvider;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.functions.Func1;

@Singleton
public class RxGeoCoder implements Func1<Location, String> {

    private static final String TAG = "RxGeocoder";
    private final Geocoder geocoder;
    private final ResourceProvider resourceProvider;

    @Inject
    public RxGeoCoder(Context context, ResourceProvider resourceProvider) {
        this.geocoder = new Geocoder(context);
        this.resourceProvider = resourceProvider;
    }

    @Override
    public String call(Location location) {
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            Log.e(TAG, "GetFromLocation failed unable to geocode your position ",e);
        }

        if(addresses == null || addresses.isEmpty()){
            Log.e(TAG, "Address empty unable to geocode your position ");
            return resourceProvider.getString(R.string.current_location);
        }
        return addresses.get(0).getLocality();
    }
}
