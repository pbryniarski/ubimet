package com.bryn.ubimet.location;

import android.location.LocationManager;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelbryniarski on 22.11.15.
 */
@Singleton
public class LocationProviderChooser {

    private final LocationManager locationManager;

    @Inject
    public LocationProviderChooser(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    public String getLocationProvider() {

        List<String> enabledProviders = locationManager.getProviders(true);

        if(enabledProviders.contains(LocationManager.NETWORK_PROVIDER)){
            return LocationManager.NETWORK_PROVIDER;
        }

        if(enabledProviders.contains(LocationManager.GPS_PROVIDER)){
            return LocationManager.GPS_PROVIDER;
        }

        return null;
    }
}
