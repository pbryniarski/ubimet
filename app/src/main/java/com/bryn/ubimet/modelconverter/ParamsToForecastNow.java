package com.bryn.ubimet.modelconverter;

import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.network.model.PinpointTimezoneForecast;
import com.bryn.ubimet.network.model.PoiTimezoneForecast;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelbryniarski on 15.11.15.
 */
@Singleton
public class ParamsToForecastNow {

    @Inject
    public ParamsToForecastNow() {
    }

    public ForecastNow getPinpointForecast(PinpointTimezoneForecast pinpointTimezoneForecast) {
        return getForecast(pinpointTimezoneForecast.met_sets[0].parameter_timesets[0].data[0], "Current location");
    }

    public ForecastNow getForecast(double[][] parameterValues, String locationName) {
        return new ForecastNow(
                locationName,
                parameterValues[0][0],
                parameterValues[1][0],
                parameterValues[2][0],
                parameterValues[3][0],
                parameterValues[4][0],
                parameterValues[5][0],
                parameterValues[6][0],
                (int) parameterValues[7][0],
                parameterValues[8][0],
                parameterValues[9][0]
        );
    }

    public Collection<? extends ForecastNow> getPoiForecasts(PoiTimezoneForecast poiForecast, Map<String, String> info) {
        List<ForecastNow> forecastNows = new LinkedList<>();
        double[][][] parameterValues = poiForecast.met_sets[0].parameter_timesets[0].data;
        for (int i = 0; i < parameterValues.length; i++) {
            double[][] values = parameterValues[i];
            forecastNows.add(getForecast(values, info.get(poiForecast.spatial_features.poi_refs[i])));
        }
        return forecastNows;
    }
}
