package com.bryn.ubimet.modelconverter;

import com.bryn.ubimet.network.model.PoiInfo;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.functions.Func1;

/**
 * Created by pawelbryniarski on 22.11.15.
 */
@Singleton
public class PoiInfoConverter implements Func1<PoiInfo[], Map<String, String>> {

    @Inject
    public PoiInfoConverter() {
    }

    @Override
    public Map<String, String> call(PoiInfo[] poiInfos) {
        Map<String, String> map = new HashMap<>();
        for (PoiInfo info : poiInfos) {
            for (PoiInfo.Info i : info.spatial_features) {
                map.put(i.id, i.name);
            }

        }
        return map;
    }
}
