package com.bryn.ubimet.modelconverter;

import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.network.model.PoiTimezoneForecast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.functions.Func2;

/**
 * Created by pawelbryniarski on 22.11.15.
 */
@Singleton
public class PoiForecastConverter implements Func2<PoiTimezoneForecast[], Map<String, String>, ArrayList<ForecastNow>> {

    private final ParamsToForecastNow paramsToForecastNow;

    @Inject
    public PoiForecastConverter(ParamsToForecastNow paramsToForecastNow) {
        this.paramsToForecastNow = paramsToForecastNow;
    }

    @Override
    public ArrayList<ForecastNow> call(PoiTimezoneForecast[] poiTimezoneForecasts, Map<String, String> idsToNames) {
        ArrayList<ForecastNow> forecasts = new ArrayList<>();
        for (PoiTimezoneForecast poiForecast : poiTimezoneForecasts) {
            forecasts.addAll(paramsToForecastNow.getPoiForecasts(poiForecast, idsToNames));
        }
        return forecasts;
    }
}
