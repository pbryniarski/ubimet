package com.bryn.ubimet.modelconverter;

import android.util.Log;

import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.network.model.PinpointTimezoneForecast;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.functions.Func1;

/**
 * Created by pawelbryniarski on 22.11.15.
 */
@Singleton
public class PinpointForecastConverter implements Func1<PinpointTimezoneForecast[], ForecastNow> {

    private static final String TAG = "PinpointForecastConv";
    private final ParamsToForecastNow paramsToForecastNow;

    @Inject
    public PinpointForecastConverter(ParamsToForecastNow paramsToForecastNow) {
        this.paramsToForecastNow = paramsToForecastNow;
    }

    @Override
    public ForecastNow call(PinpointTimezoneForecast[] timezoneForecast) {
        Log.d(TAG,"Forecast to pinpoint");
        return paramsToForecastNow.getPinpointForecast(timezoneForecast[0]);
    }
}
