package com.bryn.ubimet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.bryn.ubimet.App;
import com.bryn.ubimet.R;
import com.bryn.ubimet.datamodel.ForecastNow;
import com.bryn.ubimet.util.ResourceProvider;
import com.bryn.ubimet.view.FullForecastView;

import javax.inject.Inject;

public class FullForecastActivity extends AppCompatActivity {

    private static final String FORECAST = "FORECAST";
    private static final String TAG = "FullForecastActivity";

    private FullForecastView view;

    @Inject
    ResourceProvider resourceProvider;

    public static Intent getIntent(Context context, ForecastNow forecastNow){

        Intent intent = new Intent(context, FullForecastActivity.class);
        intent.putExtra(FORECAST, forecastNow);
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_forecast);

        ((App) getApplication()).getApplicationComponent().inject(this);
        Bundle data = getIntent().getExtras();
        ForecastNow forecastNow = data.getParcelable(FORECAST);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(resourceProvider.getFullForecastTitle());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        view = new FullForecastView(findViewById(R.id.full_forecast_view),
                resourceProvider,
                getLayoutInflater());
        view.bindViews(forecastNow);
                Log.d(TAG, forecastNow.toString());
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {

        int itemId = item.getItemId();

        if(itemId == android.R.id.home){
            onBackPressed();
        }
        return true;
    }
}
