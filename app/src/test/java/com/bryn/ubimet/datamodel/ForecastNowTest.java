package com.bryn.ubimet.datamodel;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ForecastNowTest {

    private static final String LOCATION_NAME = "TEST NAME";
    private static final double FEEL_TEMPERATURE = 273.1;
    private static final double WIND_GUST = 100;
    private static final double TEMPERATURE = 293.1;
    private static final double CLOUD_COVER = 1;
    private static final double HUMIDITY = 1;
    private static final double PRECIPITATION = 10;
    private static final double WIND_DIRECTION = 0;
    private static final double WIND_SPEED = 12;
    private static final double PRESSURE = 1000;

    private ForecastNow forecastNow;

    @Before
    public void setUp() throws Exception {
        forecastNow = new ForecastNow(
                LOCATION_NAME,
                FEEL_TEMPERATURE,
                WIND_GUST,
                TEMPERATURE,
                CLOUD_COVER,
                HUMIDITY,
                PRECIPITATION,
                WIND_DIRECTION,
                1,
                WIND_SPEED,
                PRESSURE);
    }

    @Test
    public void testCorrectLocation() throws Exception {
        assertEquals(LOCATION_NAME, forecastNow.locationName);
    }

    @Test
    public void testFeelTemperature() throws Exception {
        assertEquals("0.0", forecastNow.feelTemperature);
    }

    @Test
    public void testWindGustSpeed() throws Exception {
        assertEquals(String.valueOf(WIND_GUST), forecastNow.windGustSpeed);
    }

    @Test
    public void testTemperature() throws Exception {
        assertEquals("20.0", forecastNow.temperature);
    }

    @Test
    public void testCloudCover() throws Exception {
        assertEquals("100", forecastNow.cloudCover);
    }

    @Test
    public void testHumidity() throws Exception {
        assertEquals("100", forecastNow.humidity);
    }

    @Test
    public void testPrecipitation() throws Exception {
        assertEquals(String.valueOf(PRECIPITATION), forecastNow.precipitation);
    }

    @Test
    public void testWindDirection() throws Exception {
        assertEquals("N", forecastNow.windDirection);
    }

    @Test
    public void testWindSpeed() throws Exception {
        assertEquals(String.valueOf(WIND_SPEED), forecastNow.windSpeed);
    }

    @Test
    public void testPressure() throws Exception {
        assertEquals(String.valueOf(PRESSURE), forecastNow.pressure);
    }
}