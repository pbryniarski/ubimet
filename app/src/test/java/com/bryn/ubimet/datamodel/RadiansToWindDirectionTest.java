package com.bryn.ubimet.datamodel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class RadiansToWindDirectionTest {

    @Parameterized.Parameters
    public static Collection radiansToWindDirection() {
        return Arrays.asList(new Object[][]{
                {0, "N"},
                {0.25 * 2 * 3.14, "E"},
                {0.5 * 2 * 3.14 , "S"},
                {0.75 * 2 * 3.14 , "W"}
        });
    }

    private double radians;
    private String windDirection;

    public RadiansToWindDirectionTest(double radians, String windDirection) {
        this.radians = radians;
        this.windDirection = windDirection;
    }

    @Test
    public void testConversion() throws Exception {
        assertEquals(windDirection, ForecastNow.radiansToWindDirectionString(radians));
    }
}
